import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Day04plus_Exercise04 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        String url = "https://tiemchungcovid19.gov.vn/portal/register-person";
        String browser = "Firefox";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        verifyCheckbox();
        Thread.sleep(3000);
        driver.quit();
    }

    public static void verifyCheckbox() {
        WebElement checkBox = driver.findElement(By.xpath("(//div[@class='mat-radio-outer-circle'])[2]"));
        checkBox.click();
        boolean statusCheckbox = checkBox.isDisplayed();
        if (statusCheckbox == true) {
            System.out.println("Checkbox 'Đăng ký cho người thân' đã được chọn");
        } else {
            System.out.println("Checkbox 'Đăng ký cho người thân' không được chọn");
        }
    }
}

