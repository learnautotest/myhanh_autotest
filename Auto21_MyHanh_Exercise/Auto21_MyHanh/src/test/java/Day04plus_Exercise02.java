import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Set;

public class Day04plus_Exercise02 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        String browser = "Chorme";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        verifyMessage();
        Thread.sleep(3000);
        driver.quit();
    }

    public static void verifyMessage() throws InterruptedException {
        String url = "https://shopee.vn/buyer/login";
        driver.get(url);
        driver.manage().window().maximize();
        System.out.println(driver.getTitle());
        WebElement clickGG = driver.findElement(By.xpath("//div[text()='Google']"));
        clickGG.click();
        String parentIDTab = driver.getWindowHandle();
        System.out.println(parentIDTab);

        Set<String> listIdTab = driver.getWindowHandles();
        for (String item : listIdTab) {
            driver.switchTo().window(item);
            String title = driver.getTitle();
            Thread.sleep(2000);
            if (title.equals("Sign in - Google Accounts")) {
                WebElement clickNext = driver.findElement(By.xpath("//span[text()='Next']"));
                clickNext.click();
                WebElement verifyMessage = driver.findElement(By.xpath("(//div[@jsname='B34EJ'])[1]"));
                boolean status = verifyMessage.isDisplayed();
                if (status == true) {
                    System.out.println("Message lỗi được hiển thị");
                } else {
                    System.out.println("Message lỗi không được hiển thị");
                }
                break;
            }
        }



    }
}

