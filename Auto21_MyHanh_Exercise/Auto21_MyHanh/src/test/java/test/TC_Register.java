package test;

import core.BaseTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.ReadFile;
import pages.Obj.RegisterPageObj;
import pages.Obj.ReadExcelFileObj;

import java.io.IOException;

public class TC_Register extends BaseTest {
    RegisterPageObj objRegister;
    ReadExcelFileObj objExcelRegister;
    @BeforeClass
    public void openUrl() {
        String url = BaseUrl + "telecom/addcustomer.php";
        driver.get(url);
        objRegister = new RegisterPageObj(driver);
        objExcelRegister = new ReadExcelFileObj();
    }

    @DataProvider
    public Object[][] dataCustomer() throws IOException {
        Object[][] dataCustomer = objExcelRegister.getTable();
        return dataCustomer;
    }

    @Test(dataProvider = "dataCustomer")
    public void TC_FinalProject(String FirstName, String LastName, String Email, String Address, String Phone, String Message) throws InterruptedException {
        objRegister.enterToFirstNameTxT(FirstName);
        objRegister.enterToLastNameTxT(LastName);
        objRegister.enterToEmailTxT(Email);
        objRegister.enterToAddressTxT(Address);
        objRegister.enterToPhoneTxT(Phone);
        objRegister.clickToBackground();
        if (Message=="")
        {
            objRegister.clickToSubmit();
            objRegister.clickToMenu();
            objRegister.clickToAddCustomer();
        }
        else if (objRegister.GetErrorMsgState(Message))
        {
            objRegister.clickToReset();
        }
        else {
            objRegister.clickToReset();
        }
        Thread.sleep(1000);
    }

}
