import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Day04_Exercise04 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        String url = "https://netbanking.hdfcbank.com/netbanking/";
        String browser = "Chorme";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        inputCus();
        Thread.sleep(3000);
        driver.quit();
    }

    public static void inputCus() throws InterruptedException {
        WebElement frame = driver.findElement(By.xpath("//frame[@name='login_page']"));
        driver.switchTo().frame(frame);
        WebElement btnCus = driver.findElement(By.xpath("//input[@type='text']"));
        btnCus.sendKeys("aaaaa");
        Thread.sleep(3000);
        WebElement clickCont = driver.findElement((By.xpath("//a[text()='CONTINUE']")));
        clickCont.click();
        System.out.println(btnCus.getAttribute("value").equals("aaaaa"));

    }
}

