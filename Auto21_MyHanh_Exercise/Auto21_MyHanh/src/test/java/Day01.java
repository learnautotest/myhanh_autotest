import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Day01 {
        static WebDriver driver;
        static String path = System.getProperty("user.dir");

        public static void main(String[] args) throws InterruptedException {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.get("https://www.google.com");

            Dimension newSize = new Dimension(750, 800);
            driver.manage().window().setSize(newSize);

            Point p = driver.manage().window().getPosition();
            System.out.println("Default position = " + p);

            Point point = new Point(50, 100);
            driver.manage().window().setPosition(point);
            System.out.println("New position = " + point);

            Thread.sleep(3000);
            driver.get("https://tiki.vn/");

    }
}
