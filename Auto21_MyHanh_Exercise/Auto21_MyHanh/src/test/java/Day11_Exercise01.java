import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class Day11_Exercise01 {
    WebDriver driver;

    @BeforeClass
    public void initBrowser() {
        String url = "https://tiki.vn/";
        String browser = "Chrome";
        switch (browser) {
            case "Chrome":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
    }

    @BeforeMethod
    public void refresh() {
        driver.navigate().refresh();
    }

    @Test
    public void Search_TC01() throws InterruptedException {
        WebElement inputProduct = driver.findElement(By.xpath("//input[@data-view-id='main_search_form_input']"));
        inputProduct.sendKeys("Laptop");
        WebElement clickSearch = driver.findElement(By.xpath("//button[text()='Tìm kiếm']"));
        clickSearch.click();

        WebDriverWait wait = new WebDriverWait(driver, java.time.Duration.ofSeconds(30));
        WebElement clickFirstProduct = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h3[text()='Laptop Dell Latitude 3420 L3420I5SSD (Core i5-1135G7/ 8GB/ 256GB SSD/ 14 HD/ Fedora) - Hàng Chính Hãng'])[1]")));
        clickFirstProduct.click();

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1800)");

        Thread.sleep(5000);

        WebElement verifyThuongHieu1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Thương hiệu']")));
        Assert.assertTrue(verifyThuongHieu1.isDisplayed());

        WebElement verifyXXTH1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Xuất xứ thương hiệu']")));
        Assert.assertTrue(verifyXXTH1.isDisplayed());

        WebElement verifyXuatXu1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Xuất xứ']")));
        Assert.assertTrue(verifyXuatXu1.isDisplayed());
    }

    @Test
    public void Search_TC02() throws InterruptedException {
        WebElement inputProduct = driver.findElement(By.xpath("//input[@data-view-id='main_search_form_input']"));
        inputProduct.sendKeys("Sữa tươi không đường");
        WebElement clickSearch = driver.findElement(By.xpath("//button[text()='Tìm kiếm']"));
        clickSearch.click();

        WebDriverWait wait = new WebDriverWait(driver, java.time.Duration.ofSeconds(30));
        WebElement clickSecondProduct = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//h3[text()='Thùng 48 Hộp Sữa Tươi Tiệt Trùng Dutch Lady Cô Gái Hà Lan Không Đường (48X180ml)'])[1]")));
        clickSecondProduct.click();
        Thread.sleep(2000);

        JavascriptExecutor js1 = (JavascriptExecutor) driver;
        js1.executeScript("window.scrollBy(0,1800)");
        Thread.sleep(2000);

        WebElement verifyThuongHieu2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Thương hiệu']")));
        Assert.assertTrue(verifyThuongHieu2.isDisplayed());

        WebElement verifyXXTH2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Xuất xứ thương hiệu']")));
        Assert.assertTrue(verifyXXTH2.isDisplayed());

        WebElement verifyXuatXu2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Xuất xứ']")));
        Assert.assertTrue(verifyXuatXu2.isDisplayed());

        WebElement verifyHSD = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Hạn sử dụng']")));
        Assert.assertTrue(verifyHSD.isDisplayed());

    }
}

