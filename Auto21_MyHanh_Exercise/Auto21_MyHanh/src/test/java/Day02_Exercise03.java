import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;

import javax.swing.*;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class Day02_Exercise03 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        String url = "https://demoqa.com/select-menu";
        String browser = "Chorme";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        SelectValue();
        //SelectOne();
        sleep(3000);
        driver.quit();
    }

    public static void SelectValue() throws InterruptedException {
        WebElement btnOption = driver.findElement(By.xpath(" //div[@id='withOptGroup']"));
        btnOption.click();

        WebElement btn1 = driver.findElement(By.xpath(" //div[@id='react-select-2-option-0-0']"));
        WebElement btn2 = driver.findElement(By.xpath(" //div[@id='react-select-2-option-0-1']"));
        WebElement btn3 = driver.findElement(By.xpath(" //div[@id='react-select-2-option-1-0']"));
        WebElement btn4 = driver.findElement(By.xpath(" //div[@id='react-select-2-option-1-1']"));
        WebElement btn5 = driver.findElement(By.xpath(" //div[@id='react-select-2-option-2']"));
        WebElement btn6 = driver.findElement(By.xpath(" //div[@id='react-select-2-option-3']"));

        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].scrollIntoView(true);", btn6);
        js.executeScript("arguments[0].setAttribute('class', ' css-yt9ioa-option')",btn1);
        js.executeScript("arguments[0].setAttribute('class', ' css-yt9ioa-option')",btn2);
        js.executeScript("arguments[0].setAttribute('class', ' css-yt9ioa-option')",btn3);
        js.executeScript("arguments[0].setAttribute('class', ' css-yt9ioa-option')",btn4);
        js.executeScript("arguments[0].setAttribute('class', ' css-yt9ioa-option')",btn5);
        js.executeScript("arguments[0].setAttribute('class', ' css-1n7v3ny-option')",btn6);
        Thread.sleep(3000);
        btn6.click();
        Thread.sleep(3000);
    }
    public static void SelectOne() throws InterruptedException {
        WebElement btnOption = driver.findElement(By.xpath(" //div[@id='selectOne']"));
        btnOption.click();
        WebElement[] b = new WebElement[6];
        b[0] = driver.findElement(By.xpath(" //div[@id='react-select-3-option-0-0']"));
        b[1] = driver.findElement(By.xpath(" //div[@id='react-select-3-option-0-1']"));
        b[2] = driver.findElement(By.xpath(" //div[@id='react-select-3-option-0-2']"));
        b[3] = driver.findElement(By.xpath(" //div[@id='react-select-3-option-0-3']"));
        b[4] = driver.findElement(By.xpath(" //div[@id='react-select-3-option-0-4']"));
        b[5] = driver.findElement(By.xpath(" //div[@id='react-select-3-option-0-5']"));
        Random random = new Random();
        int index = random.nextInt(6);
        JavascriptExecutor js = (JavascriptExecutor)driver;

        for (int i = 0;i<6;i++)
        {
            js.executeScript("arguments[0].setAttribute('class', ' css-yt9ioa-option')",b[i]);
        }
        js.executeScript("arguments[0].setAttribute('class', ' css-1n7v3ny-option')",b[index]);
        Thread.sleep(3000);
        b[index].click();
        Thread.sleep(3000);
    }
}

