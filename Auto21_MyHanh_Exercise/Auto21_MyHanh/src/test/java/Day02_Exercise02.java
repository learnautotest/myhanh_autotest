import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class Day02_Exercise02 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        String url = "https://demo.guru99.com/test/upload/";
        String browser = "Chorme";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        clickTerms();
        Thread.sleep(3000);
        driver.quit();
    }

    public static void clickTerms() {
        WebElement Terms = driver.findElement(By.id("terms"));
        Terms.click();
        WebElement Submit = driver.findElement(By.id("submitbutton"));
        Submit.click();
    }

}

