import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Collections;
import java.util.Random;
import java.util.ArrayList;
import java.util.function.Consumer;

public class Day04plus_Exercise07 {
    static WebDriver driver;

    @BeforeClass
    public static void main(String[] args) throws InterruptedException {
        String url = "https://login.mailchimp.com/signup/";
        String browser = "Chorme";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        InputEmail();
        InputPassword_01();
        InputPassword_02();
        InputPassword_03();
        InputPassword_04();
    }

    @BeforeMethod
    public void refresh() {
        driver.navigate().refresh();
    }

    @Test
    public static void InputEmail() throws InterruptedException {
        String email = generateRandomEmail();
        WebElement txtEmail = driver.findElement(By.id("email"));
        txtEmail.sendKeys(email);
        Thread.sleep(3000);
    }

    public static String generateRandomEmail() {
        String allowedChars = "abcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder email = new StringBuilder();
        Random random = new Random();
        int index;
        for (int i = 0; i < 10; i++) {
            index = random.nextInt(allowedChars.length());
            email.append(allowedChars.charAt(index));
        }
        email.append("@gmail.com");
        return email.toString();
    }

    @Test
    public static void InputPassword_01() throws InterruptedException {
        String password = randomAlphaNumeric();
        WebElement txtPassword = driver.findElement(By.id("new_password"));
        txtPassword.sendKeys(password);
        boolean status = txtPassword.isDisplayed();
        if (status == true) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
        Thread.sleep(2000);
        txtPassword.clear();
    }
    public static String randomAlphaNumeric() {
        String digits = "0123456789";
        StringBuilder password = new StringBuilder();
        Random random = new Random();
        int index;
        for (int i = 1; i <= 8; i++) {
            index = random.nextInt(digits.length());
            password.append(digits.charAt(index));
        }
        return password.toString();
    }

    @Test
    public static void InputPassword_02() throws InterruptedException {
        String password = randomAlphaUpperCase();
        WebElement txtPassword = driver.findElement(By.id("new_password"));
        txtPassword.sendKeys(password);
        boolean status = txtPassword.isDisplayed();
        if (status == true) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
        Thread.sleep(2000);
        txtPassword.clear();
    }
    public static String randomAlphaUpperCase() {
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        String alphaUpperCase = alpha.toUpperCase();
        StringBuilder Upper = new StringBuilder();
        Random random = new Random();
        int index;
        for (int i = 0; i <= 10; i++) {
            index = random.nextInt(alphaUpperCase.length());
            Upper.append(alphaUpperCase.charAt(index));
        }
        return Upper.toString();
    }

    @Test
    public static void InputPassword_03() throws InterruptedException {
        String password = randomSpecials();
        WebElement txtPassword = driver.findElement(By.id("new_password"));
        txtPassword.sendKeys(password);
        boolean status = txtPassword.isDisplayed();
        if (status == true) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
        Thread.sleep(2000);
        txtPassword.clear();
    }
    public static String randomSpecials() {
        String characterspecials = "~=+%^*/()[]{}/!@#$?|";
        StringBuilder specials = new StringBuilder();
        Random random = new Random();
        int index;
        for (int i = 0; i <= 10; i++) {
            index = random.nextInt(characterspecials.length());
            specials.append(characterspecials.charAt(index));
        }
        return specials.toString();
    }

    @Test
    public static void InputPassword_04() throws InterruptedException {
        String password = randomALL();
        WebElement txtPassword = driver.findElement(By.id("new_password"));
        txtPassword.sendKeys(password);
        boolean status = txtPassword.isDisplayed();
        if (status == true) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
        Thread.sleep(2000);
    }
    public static String randomALL() {
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        String alphaUpperCase = alpha.toUpperCase();
        String digits = "0123456789";
        String specials = "~=+%^*/()[]{}/!@#$?|";
        String ALL = alphaUpperCase + digits + specials;
        StringBuilder AllCharacter = new StringBuilder();
        Random random = new Random();
        int index;
        for (int i = 0; i <= 10; i++) {
            index = random.nextInt(ALL.length());
            AllCharacter.append(ALL.charAt(index));
        }
        return AllCharacter.toString();
    }

    @AfterClass
    public void AfterClass() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }
}

