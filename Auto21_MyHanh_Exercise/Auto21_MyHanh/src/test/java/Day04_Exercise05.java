import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class Day04_Exercise05 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        String url = "https://demo.guru99.com/test/drag_drop.html";
        String browser = "Chorme";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        dragDrop();
        Thread.sleep(3000);
        driver.quit();
    }

    public static void dragDrop() throws InterruptedException {
        WebElement from1 = driver.findElement(By.id("credit2"));
        WebElement to1 = driver.findElement(By.id("bank"));
        WebElement from2 = driver.findElement(By.id("credit1"));
        WebElement to2 = driver.findElement(By.id("loan"));
        WebElement from3 = driver.findElement(By.xpath("(//li[@id='fourth'])[1]"));
        WebElement to3 = driver.findElement(By.id("amt7"));
        WebElement from4 = driver.findElement(By.xpath("(//li[@id='fourth'])[2]"));
        WebElement to4 = driver.findElement(By.id("amt8"));


        Actions builder = new Actions(driver);
        Action dragAndDrop1 = builder.clickAndHold(from1).moveToElement(to1).release(to1).build();
        Action dragAndDrop2 = builder.clickAndHold(from2).moveToElement(to2).release(to2).build();
        Action dragAndDrop3 = builder.clickAndHold(from3).moveToElement(to3).release(to3).build();
        Action dragAndDrop4 = builder.clickAndHold(from4).moveToElement(to4).release(to4).build();

        dragAndDrop1.perform();
        Thread.sleep(2000);
        dragAndDrop2.perform();
        Thread.sleep(2000);
        dragAndDrop3.perform();
        Thread.sleep(2000);
        dragAndDrop4.perform();
        Thread.sleep(2000);
    }
}

