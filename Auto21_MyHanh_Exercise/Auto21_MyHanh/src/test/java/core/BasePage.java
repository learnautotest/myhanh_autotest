package core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Random;
public class BasePage {
    private long TIMEOUT = 30;
    private WebDriverWait wait;
    private Actions actions;

    public By getByXpath(String locator){
        return By.xpath(locator);
    }
    public By getByID(String locator){
        return By.id(locator);
    }
    public WebElement getElementByXpath (WebDriver driver, String locator){
        return driver.findElement(getByXpath(locator));
    }
    public boolean getDisplayState (WebDriver driver, String text){
        return getElementByXpath(driver,"//label[text()='"+text+"']").isDisplayed();
    }
    public WebElement getElementByID (WebDriver driver, String locator){
        return driver.findElement(getByID(locator));
    }
    public void clickToElement(WebDriver driver, String locator){
        getElementByXpath(driver, locator).click();
    }
    public void sendKeyToElementByXpath(WebDriver driver, String locator, String text){
        getElementByXpath(driver, locator).sendKeys(text);
    }
    public void waitForElementVisible(WebDriver driver, String locator){
        wait = new WebDriverWait(driver, Duration.ofSeconds(TIMEOUT));
        wait.until(ExpectedConditions.visibilityOfElementLocated(getByXpath(locator)));
    }
    public void doubleClickToElement(WebDriver driver, String locator) {
        actions = new Actions(driver);
        actions.doubleClick(getElementByXpath(driver, locator)).build().perform();
    }
    public static String randomAlpha(int sokitu) {
        String alpha = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ";
        StringBuilder Upper = new StringBuilder();
        Random random = new Random();
        int index;
        for (int i = 0; i < sokitu; i++) {
            index = random.nextInt(alpha.length());
            Upper.append(alpha.charAt(index));
        }
        return Upper.toString();
    }
    public static String randomAlphaNumeric() {
        String digits = "0123456789";
        StringBuilder phone = new StringBuilder();
        Random random = new Random();
        phone.append("0");
        for (int i = 1; i <= 8; i++) {
            int index = random.nextInt(digits.length()-2);
            phone.append(digits.charAt(index));
        }
        return phone.toString();
    }

}
