package core;

import core.BasePage;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Random;

public class ReadFile extends BasePage {
    private static XSSFWorkbook excelWBook;
    private static XSSFSheet excelSheet;
    private static XSSFCell excelCell;
    private static XSSFRow excelRow;
    private static String Filepath;
    private static String Sheetname;

    public static void Inputdata(String fp, String sn){
        Filepath = fp;
        Sheetname = sn;
    }

    //read data from excel
    public static String getCellData(int row, int col) {
        excelCell = excelSheet.getRow(row).getCell(col);
        String dataCell = excelCell.getStringCellValue();
        return dataCell;
    }

    public Object[][] getTableArray() throws IOException {
        String[][] newTableArray = null; //khai báo mảng 2 chiều
        FileInputStream excelFile = new FileInputStream(Filepath);
        //truy cập vào file data test, file excel và sheet chứa data
        excelWBook = new XSSFWorkbook(excelFile);
        excelSheet = excelWBook.getSheet(Sheetname);
        int startRow = 1;
        int startCol = 1;
        int ci, cj; //tọa độ mới sau khi bỏ cột A và header
        int totalRows = excelSheet.getLastRowNum(); //tính từ cột đầu tiên bắt đầu từ 0
        int totalCols = excelSheet.getRow(1).getLastCellNum() - 1; //dòng 1 mới chứa data, trả về đúng giá trị của cột
        System.out.println("totalRow: " + totalRows);
        System.out.println("totalCols: " + totalCols);

        newTableArray = new String[totalRows][totalCols];
        int slfirstname, sllastname, slemail, sladdress, slmsg;
        for (int i = startRow; i <= totalRows; i++) {
            ci = i - 1;
            for (int j = startCol; j <= totalCols; j++) {
                cj = j - 1;
                System.out.println("ci: " + ci);
                System.out.println("cj: " + cj);
                if (j==1)// cot first name
                {
                    if (getCellData(i, j).split("_").length != 2) {
                        newTableArray[ci][cj] = getCellData(i, j);
                    } else {
                        slfirstname = Integer.parseInt(getCellData(i, j).split("_")[1]);
                        newTableArray[ci][cj] = randomAlpha(slfirstname);
                    }
                }
                else if (j==2)// cot last name
                {
                    if (getCellData(i, j).split("_").length != 2) {
                        newTableArray[ci][cj] = getCellData(i, j);
                    } else {
                        sllastname = Integer.parseInt(getCellData(i, j).split("_")[1]);
                        newTableArray[ci][cj] = randomAlpha(sllastname);
                    }
                }
                else if (j==3)// cot email
                {
                    if (getCellData(i, j).split("_").length != 2) {
                        newTableArray[ci][cj] = getCellData(i, j);
                    } else {
                        slemail = Integer.parseInt(getCellData(i, j).split("_")[1]);
                        newTableArray[ci][cj] = randomAlpha(slemail) + "@gmail.com";
                    }
                }
                else if (j==4)// cot address
                {
                    if (getCellData(i, j).split("_").length != 2) {
                        newTableArray[ci][cj] = getCellData(i, j);
                    } else {
                        sladdress = Integer.parseInt(getCellData(i, j).split("_")[1]);
                        newTableArray[ci][cj] = randomAlpha(sladdress);
                    }
                }
                else if (j==5)// cot phone
                {
                    if (Objects.equals(getCellData(i, j), "")) {
                        newTableArray[ci][cj] = "";
                    } else {
                        newTableArray[ci][cj] = randomAlphaNumeric();
                    }
                }
                else if (j==6)// cot message
                {
                    if (getCellData(i, j).split("_").length != 2) {
                        newTableArray[ci][cj] = getCellData(i, j);
                    } else {
                        slmsg = Integer.parseInt(getCellData(i, j).split("_")[1]);
                        newTableArray[ci][cj] = randomAlpha(slmsg);
                    }
                }
                System.out.println(newTableArray[ci][cj]);
            }

        }
        return newTableArray;
    }
}
