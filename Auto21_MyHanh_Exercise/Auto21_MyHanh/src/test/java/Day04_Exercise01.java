import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Set;

import static java.lang.Thread.sleep;

public class Day04_Exercise01 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        String browser = "Chorme";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        switchToPage();
        Thread.sleep(20000);
        driver.quit();
    }

    public static void switchToPage() throws InterruptedException {
        String url = "https://dictionary.cambridge.org/vi/";
        driver.get(url);
        driver.manage().window().maximize();
        System.out.println(driver.getTitle());
        WebElement btnSignIn = driver.findElement(By.xpath("//div[@class='pr hdib lpr-5']//span[text()='Đăng nhập']"));
        btnSignIn.click();
        sleep(10000);
        String parentIDTab = driver.getWindowHandle();
        System.out.println(parentIDTab);

        Set<String> listIdTab = driver.getWindowHandles();
        for (String item : listIdTab) {
            driver.switchTo().window(item);
            String title = driver.getTitle();
            if (title.equals("Login")) {
                WebElement btnLogIn = driver.findElement(By.xpath("(//input[@type='submit'])[7]"));
                btnLogIn.click();
                sleep(20000);
                WebElement verifyEmail = driver.findElement(By.xpath("(//span[text()='This field is required'])[1]"));
                boolean status = verifyEmail.isDisplayed();
                if (status == true) {
                    System.out.println("'This field is required' được hiển thị");
                } else {
                    System.out.println("'This field is required' không được hiển thị");
                }
                break;
            }
        }
        driver.switchTo().window(parentIDTab);
        System.out.println("Đã chuyển về Main Window: " + driver.getCurrentUrl());
        sleep(3000);
    }
}

