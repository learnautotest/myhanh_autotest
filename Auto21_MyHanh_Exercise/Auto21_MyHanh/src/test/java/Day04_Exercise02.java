import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Set;

public class Day04_Exercise02 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        String url = "https://demoqa.com/frames";
        String browser = "Chorme";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        switchtoFrame();
        Thread.sleep(3000);
        driver.quit();
    }

    public static void switchtoFrame() {
        driver.switchTo().frame("frame1");
        WebElement frame1 = driver.findElement(By.id("sampleHeading"));
        System.out.println(frame1.getText());
        driver.switchTo().parentFrame();
        driver.switchTo().frame("frame2");
        WebElement frame2 = driver.findElement(By.id("sampleHeading"));
        System.out.println(frame2.getText());
        driver.switchTo().parentFrame();
    }
}

