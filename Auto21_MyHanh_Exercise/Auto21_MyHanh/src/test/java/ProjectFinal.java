import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class ProjectFinal {
    WebDriver driver;
    @BeforeClass
    public void initBrowser() {
        String url = "https://demo.guru99.com/telecom/addcustomer.php";
        String browser = "Chrome";
        switch (browser) {
            case "Chrome":
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--lang=en");
                driver = WebDriverManager.chromedriver().capabilities(options).create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        driver.manage().window().maximize();
    }
    @DataProvider
    public Object[][] dataCustomer() throws IOException {
        String path = System.getProperty("user.dir");
        String filePath = path + "\\src\\test\\resources\\FinalTest.xlsx";
        String nameSheet = "AddNewCustomer";
        Object[][] dataCustomer = ProjectFinal_DataDrivenTesting.getTableArray(filePath, nameSheet);
        return dataCustomer;
    }

    @Test(dataProvider = "dataCustomer")
    public void TC_Register(String FirstName, String LastName, String Email, String Address, String Phone, String Message) throws InterruptedException {
        WebElement txtFirstName = driver.findElement(By.id("fname"));
        txtFirstName.sendKeys(FirstName);
        WebElement txtLastName = driver.findElement(By.id("lname"));
        txtLastName.sendKeys(LastName);
        WebElement txtEmail = driver.findElement(By.id("email"));
        txtEmail.sendKeys(Email);
        WebElement txtAddress = driver.findElement(By.xpath("//textarea[@id='message']"));
        txtAddress.sendKeys(Address);
        WebElement txtPhone = driver.findElement(By.id("telephoneno"));
        txtPhone.sendKeys(Phone);
        WebElement backgroundWhite = driver.findElement(By.id("main"));
        backgroundWhite.click();
        WebElement clickReset = driver.findElement(By.xpath("//input[@type='reset']"));
        WebElement clickSubmit = driver.findElement(By.xpath("//input[@type='submit']"));
        if (Message == "")
        {
            clickSubmit.click();
            WebElement menu = driver.findElement(By.xpath("(//a[@href='#menu'])[1]"));
            menu.click();
            WebElement clickAddCustomer = driver.findElement(By.xpath("//a[@href='addcustomer.php']"));
            clickAddCustomer.click();
        }
        else {
            WebElement ErrorMsg = driver.findElement(By.xpath("//label[text()='"+Message+"']"));
            if (ErrorMsg.isDisplayed())
            {
                clickReset.click();
            }
            else {
                clickReset.click();
            }
        }
        Thread.sleep(1000);
    }

    @AfterClass
    public void Test() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }
}
