import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class Day04plus_Exercise01 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        String url = "https://www.w3schools.com/tags/tryit.asp?filename=tryhtml5_input_type_date";
        String browser = "Firefox";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        inputDOB();
        Thread.sleep(3000);
        driver.quit();
    }

    public static void inputDOB() throws InterruptedException {
        driver.switchTo().frame("iframeResult");
        WebElement DOB = driver.findElement(By.xpath("//input[@id='birthday']"));
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].removeAttribute('type')",DOB);
        DOB.sendKeys("08-12-1997");
        WebElement btnSubmit = driver.findElement(By.xpath("//input[@type='submit']"));
        btnSubmit.click();
        Thread.sleep(5000);
    }
}

