import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.concurrent.TimeUnit;

public class Day02_Exercise01 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        String url = "https://files.fm/";
        String browser = "Chrome";
        switch (browser) {
            case "Chrome":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        uploadFile();
        Thread.sleep(3000);
        driver.quit();
    }

    public static void uploadFile() throws InterruptedException {
        String path = System.getProperty("user.dir");
        String imageFile = path + "\\src\\test\\resources\\image.png";
        WebElement uploadFileCustom = driver.findElement(By.xpath("//input[@type='file' and @style]"));
        uploadFileCustom.sendKeys(imageFile);
        Thread.sleep(3000);
        WebElement verifyFile = driver.findElement(By.xpath("//div[@id='savefiles']"));
        verifyFile.click();
        Thread.sleep(3000);
    }
}

