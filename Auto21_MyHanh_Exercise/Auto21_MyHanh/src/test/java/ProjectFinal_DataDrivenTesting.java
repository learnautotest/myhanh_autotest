import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Random;

public class ProjectFinal_DataDrivenTesting {
    private static XSSFWorkbook excelWBook;
    private static XSSFSheet excelSheet;
    private static XSSFRow excelRow;
    private static XSSFCell excelCell;

    public static void main(String[] args) throws IOException {
        String path = System.getProperty("user.dir");
        String file = path + "\\src\\test\\resources\\FinalTest.xlsx";
        String nameSheet = "AddNewCustomer";

        getTableArray(file, nameSheet);
    }

    //read data from excel
    public static String getCellData(int row, int col) {
        excelCell = excelSheet.getRow(row).getCell(col);
        String dataCell = excelCell.getStringCellValue();
        return dataCell;
    }

    public static Object[][] getTableArray(String path, String fileName) throws IOException {
        String[][] newTableArray = null; //khai báo mảng 2 chiều
        FileInputStream excelFile = new FileInputStream(path);
        //truy cập vào file data test, file excel và sheet chứa data
        excelWBook = new XSSFWorkbook(excelFile);
        excelSheet = excelWBook.getSheet(fileName);
        int startRow = 1;
        int startCol = 1;
        int ci, cj; //tọa độ mới sau khi bỏ cột A và header
        int totalRows = excelSheet.getLastRowNum(); //tính từ cột đầu tiên bắt đầu từ 0
        int totalCols = excelSheet.getRow(1).getLastCellNum() - 1; //dòng 1 mới chứa data, trả về đúng giá trị của cột
        System.out.println("totalRow: " + totalRows);
        System.out.println("totalCols: " + totalCols);

        newTableArray = new String[totalRows][totalCols];
        int slfirstname, sllastname, slemail, sladdress, slmsg;
        for (int i = startRow; i <= totalRows; i++) {
            ci = i - 1;
            for (int j = startCol; j <= totalCols; j++) {
                cj = j - 1;
                System.out.println("ci: " + ci);
                System.out.println("cj: " + cj);
                if (j==1)// cot first name
                {
                    if (getCellData(i, j).split("_").length != 2) {
                        newTableArray[ci][cj] = getCellData(i, j);
                    } else {
                        slfirstname = Integer.parseInt(getCellData(i, j).split("_")[1]);
                        newTableArray[ci][cj] = randomAlphaUpperCase(slfirstname);
                    }
                }
                else if (j==2)// cot last name
                {
                    if (getCellData(i, j).split("_").length != 2) {
                        newTableArray[ci][cj] = getCellData(i, j);
                    } else {
                        sllastname = Integer.parseInt(getCellData(i, j).split("_")[1]);
                        newTableArray[ci][cj] = randomAlphaUpperCase(sllastname);
                    }
                }
                else if (j==3)// cot email
                {
                    if (getCellData(i, j).split("_").length != 2) {
                        newTableArray[ci][cj] = getCellData(i, j);
                    } else {
                        slemail = Integer.parseInt(getCellData(i, j).split("_")[1]);
                        newTableArray[ci][cj] = randomAlphaUpperCase(slemail) + "@gmail.com";
                    }
                }
                else if (j==4)// cot address
                {
                    if (getCellData(i, j).split("_").length != 2) {
                        newTableArray[ci][cj] = getCellData(i, j);
                    } else {
                        sladdress = Integer.parseInt(getCellData(i, j).split("_")[1]);
                        newTableArray[ci][cj] = randomAlphaUpperCase(sladdress);
                    }
                }
                else if (j==5)// cot phone
                {
                    if (Objects.equals(getCellData(i, j), "")) {
                        newTableArray[ci][cj] = "";
                    } else {
                        newTableArray[ci][cj] = randomAlphaNumeric();
                    }
                }
                else if (j==6)// cot message
                {
                    if (getCellData(i, j).split("_").length != 2) {
                        newTableArray[ci][cj] = getCellData(i, j);
                    } else {
                        slmsg = Integer.parseInt(getCellData(i, j).split("_")[1]);
                        newTableArray[ci][cj] = randomAlphaUpperCase(slmsg);
                    }
                }
                System.out.println(newTableArray[ci][cj]);
            }

            }
            return newTableArray;
        }
    public static String randomAlphaUpperCase(int sokitu) {
        String alpha = "aABbCcdefghijklmnopqrstuvwxyz";
        StringBuilder Upper = new StringBuilder();
        Random random = new Random();
        int index;
        for (int i = 0; i < sokitu; i++) {
            index = random.nextInt(alpha.length());
            Upper.append(alpha.charAt(index));
        }
        return Upper.toString();
    }
    public static String randomAlphaNumeric() {
        String digits = "0123456789";
        StringBuilder phone = new StringBuilder();
        Random random = new Random();
        phone.append("03");
        for (int i = 1; i <= 8; i++) {
            int index = random.nextInt(digits.length()-2);
            phone.append(digits.charAt(index));
        }
        return phone.toString();
    }
}