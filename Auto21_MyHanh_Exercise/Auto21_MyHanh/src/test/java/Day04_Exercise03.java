import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Set;

public class Day04_Exercise03 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {

        String browser = "Chorme";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        exercise3();
        Thread.sleep(3000);
        driver.quit();
    }
    public static void exercise3() throws InterruptedException {
        String url = "https://tiki.vn/";
        driver.get(url);
        driver.manage().window().maximize();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        Thread.sleep(1000);
        WebElement clickFB = driver.findElement(By.xpath("//a[@title='Facebook']"));
        clickFB.click();
        String parentIDTab = driver.getWindowHandle();
        System.out.println(parentIDTab);
        Set<String> listIdTab = driver.getWindowHandles();
        for (String item : listIdTab) {
            driver.switchTo().window(item);
            String title = driver.getTitle();
            System.out.println(title);
            Thread.sleep(3000);
            if (title.equals("Tiki | Facebook")) {
                WebElement textBoxEmail = driver.findElement(By.xpath("//span[text()='Email address or phone number']"));
                js.executeScript("arguments[0].click();", textBoxEmail);

                WebElement txtEmail = driver.findElement(By.xpath("//input[@type='text' and @name='email']"));
                txtEmail.sendKeys("lythimyhanh1997@gmail.com");
                Thread.sleep(1000);

                WebElement txtPassword = driver.findElement(By.xpath("//input[@type='password' and @id=':r4:']"));
                txtPassword.sendKeys("123456789");
                Thread.sleep(1000);

                WebElement btnLogin = driver.findElement(By.xpath("//div[@class='x1c436fg']//child::span[text()='Log in']"));
                btnLogin.click();
                Thread.sleep(1000);
                break;
            }
        }
    }
}

