import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Day04plus_Exercise06 {
    WebDriver driver;

    @BeforeClass
    public void initBrowser() throws InterruptedException {
        String url = "https://jqueryui.com/selectable/";
        String browser = "Chrome";
        switch (browser) {
            case "Chrome":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        driver.manage().window().maximize();
        Thread.sleep(1000);
    }

    @Test
    public void ClickAndHold() throws InterruptedException {
        WebElement iframe = driver.findElement(By.xpath("//iframe[@class='demo-frame']"));
        driver.switchTo().frame(iframe);

        WebElement Item1 = driver.findElement(By.xpath("//li[text()='Item 1']"));
        WebElement Item2 = driver.findElement(By.xpath("//li[text()='Item 2']"));
        WebElement Item3 = driver.findElement(By.xpath("//li[text()='Item 3']"));
        WebElement Item4 = driver.findElement(By.xpath("//li[text()='Item 4']"));
        WebElement Item5 = driver.findElement(By.xpath("//li[text()='Item 5']"));
        WebElement Item6 = driver.findElement(By.xpath("//li[text()='Item 6']"));
        WebElement Item7 = driver.findElement(By.xpath("//li[text()='Item 7']"));

        Actions builder = new Actions(driver);
        Action HoldItem1 = builder.clickAndHold(Item1).build();
        builder.keyDown(Keys.CONTROL).click().keyUp(Keys.CONTROL).perform();
        Action HoldItem2 = builder.clickAndHold(Item2).build();
        builder.keyDown(Keys.CONTROL).click().keyUp(Keys.CONTROL).perform();
        Action HoldItem3 = builder.clickAndHold(Item3).build();
        builder.keyDown(Keys.CONTROL).click().keyUp(Keys.CONTROL).perform();
        Action HoldItem4 = builder.clickAndHold(Item4).build();
        builder.keyDown(Keys.CONTROL).click().keyUp(Keys.CONTROL).perform();
        Action HoldItem5 = builder.clickAndHold(Item5).build();
        builder.keyDown(Keys.CONTROL).click().keyUp(Keys.CONTROL).perform();

        HoldItem1.perform();
        HoldItem2.perform();
        HoldItem3.perform();
        HoldItem4.perform();
        HoldItem5.perform();
        Thread.sleep(3000);

        String colorItem1 = driver.findElement(By.xpath("//li[text()='Item 1']")).getCssValue("background-color");
        System.out.println("Background Item 1: " + colorItem1);
        String colorItem2 = driver.findElement(By.xpath("//li[text()='Item 2']")).getCssValue("background-color");
        System.out.println("Background Item 2: " + colorItem2);
        String colorItem3 = driver.findElement(By.xpath("//li[text()='Item 3']")).getCssValue("background-color");
        System.out.println("Background Item 3: " + colorItem3);
        String colorItem4 = driver.findElement(By.xpath("//li[text()='Item 4']")).getCssValue("background-color");
        System.out.println("Background Item 4: " + colorItem4);
        String colorItem5 = driver.findElement(By.xpath("//li[text()='Item 5']")).getCssValue("background-color");
        System.out.println("Background Item 5: " + colorItem5);
        String colorItem6 = driver.findElement(By.xpath("//li[text()='Item 6']")).getCssValue("background-color");
        System.out.println("Background Item 6: " + colorItem6);
        String colorItem7 = driver.findElement(By.xpath("//li[text()='Item 7']")).getCssValue("background-color");
        System.out.println("Background Item 7: " + colorItem7);
    }
}

