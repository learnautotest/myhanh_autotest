import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.Duration;


public class Day04plus_Exercise05 {
     WebDriver driver;

    @BeforeClass
    public void initBrowser() throws InterruptedException {
        String url = "https://jqueryui.com/tooltip/";
        String browser = "Chrome";
        switch (browser) {
            case "Chrome":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        driver.manage().window().maximize();
        Thread.sleep(2000);
    }

    @Test
    public void HoverTextBox() throws InterruptedException {
        WebElement iframe = driver.findElement(By.xpath("//iframe[@class='demo-frame']"));
        driver.switchTo().frame(iframe);

        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement txtscroll = driver.findElement(By.xpath("//a[text()='Tooltips']"));
        js.executeScript("arguments[0].scrollIntoView(true);", txtscroll);

        WebElement txtAge = driver.findElement(By.xpath("//input[@id='age']"));
        Actions actions = new Actions(driver);
        actions.moveToElement(txtAge).perform();
        Thread.sleep(2000);

        WebElement tooltip = driver.findElement(By.xpath("//input[@title='We ask for your age only for statistical purposes.']"));
        Assert.assertTrue(tooltip.isDisplayed());
        Thread.sleep(3000);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}

