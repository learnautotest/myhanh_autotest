package pages.UI;

public class RegisterPage_UI {
    public static final String TXT_FIRSTNAME = "//input[@id='fname']";
    public static final String TXT_LASTNAME = "//input[@id='lname']";
    public static final String TXT_EMAIL= "//input[@id='email']";
    public static final String TXT_ADDRESS = "//textarea[@id='message']";
    public static final String TXT_PHONE = "//input[@id='telephoneno']";
    public static final String btnReset = "//input[@type='reset']";
    public static final String btnSubmit = "//input[@type='submit']";
    public static final String backgroundWhite = "//section[@id='main']";
    public static final String menu = "(//a[@href='#menu'])[1]";
    public static final String clickAddCustomer = "//a[@href='addcustomer.php']";

}
