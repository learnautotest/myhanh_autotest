package pages.Obj;

import core.BasePage;
import core.ReadFile;
import org.openqa.selenium.WebDriver;
import pages.UI.ReadExcelFile_UI;
import pages.UI.RegisterPage_UI;

import java.io.IOException;

public class ReadExcelFileObj extends ReadFile {
    public ReadExcelFileObj(){
        ReadFile.Inputdata(ReadExcelFile_UI.TXT_FILEPATHE, ReadExcelFile_UI.TXT_SHEETNAME);
    }
    public Object[][] getTable() throws IOException {
        return getTableArray();
    }
}
