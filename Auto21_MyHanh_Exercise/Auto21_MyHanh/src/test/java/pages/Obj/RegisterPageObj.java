package pages.Obj;

import core.BasePage;
import org.openqa.selenium.WebDriver;
import pages.UI.RegisterPage_UI;

public class RegisterPageObj extends BasePage {
    WebDriver driver;
    public RegisterPageObj(WebDriver driver){
        this.driver = driver;
    }
    public void enterToFirstNameTxT(String text){
        sendKeyToElementByXpath(driver, RegisterPage_UI.TXT_FIRSTNAME, text);
    }
    public void enterToLastNameTxT(String text){
        sendKeyToElementByXpath(driver, RegisterPage_UI.TXT_LASTNAME, text);
    }
    public void enterToEmailTxT(String text){
        sendKeyToElementByXpath(driver, RegisterPage_UI.TXT_EMAIL, text);
    }
    public void enterToAddressTxT(String text){
        sendKeyToElementByXpath(driver, RegisterPage_UI.TXT_ADDRESS, text);
    }
    public void enterToPhoneTxT(String text){
        sendKeyToElementByXpath(driver, RegisterPage_UI.TXT_PHONE, text);
    }
    public void clickToReset(){
        clickToElement(driver, RegisterPage_UI.btnReset);
    }
    public void clickToSubmit(){
        clickToElement(driver, RegisterPage_UI.btnSubmit);
    }
    public void clickToBackground(){
        clickToElement(driver, RegisterPage_UI.backgroundWhite);
    }
    public void clickToMenu(){
        clickToElement(driver, RegisterPage_UI.menu);
    }
    public void clickToAddCustomer(){
        clickToElement(driver, RegisterPage_UI.clickAddCustomer);
    }

    public boolean GetErrorMsgState(String errmsg){
        return getDisplayState(driver, errmsg);
    }

}
