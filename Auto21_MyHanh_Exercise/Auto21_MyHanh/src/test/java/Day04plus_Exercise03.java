import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Day04plus_Exercise03 {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        String url = "https://www.fahasa.com/customer/account/create";
        String browser = "Chorme";
        switch (browser) {
            case "Chorme":
                driver = WebDriverManager.chromedriver().create();
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                System.out.println("Chưa chọn browser");
                break;
        }
        driver.get(url);
        verifyButton();
        Thread.sleep(3000);
        driver.quit();
    }

    public static void verifyButton() {
        WebElement Tab = driver.findElement(By.xpath("//a[text()='Đăng nhập']"));
        Tab.click();
        WebElement buttonDisable = driver.findElement(By.xpath("//button[@class='fhs-btn-login']"));
        boolean status = buttonDisable.isEnabled();
        if (status == true) {
            System.out.println("Enable");
        } else {
            System.out.println("Disable");
        }
        WebElement color = driver.findElement(By.xpath("//button[@class='fhs-btn-login']"));
        String colorButton = color.getCssValue("background-color");
        System.out.println("Background button: " + colorButton);
    }
}

